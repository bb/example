package main

import (
	"fmt"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

type Person struct {
	//`bson:"_id"` 映射到Mongo id字段
	Id   bson.ObjectId `bson:"_id"`
	Name string
	Age  int
}

func main() {
	person := &Person{
		Id:   bson.NewObjectId(),
		Name: "Kaibb",
		Age:  24,
	}
	//链接到本地mongodb服务
	session, err := mgo.Dial("mongodb://127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	fmt.Println("修改mongo数据")
	c := session.DB("Tests").C("people")
	p := Person{}
	//插入两条数据
	c.Insert(person)
	//根据Name 查找
	c.Find(bson.M{"name": "Kaibb"}).One(&p)
	fmt.Println(p)
	//通过func (*Collection) Update来进行修改操作。注意修改单个或多个字段需要通过$set操作符号，否则集合会被替换。
	//修改年龄
	c.Update(bson.M{"name": "Kaibb"}, bson.M{"$set": bson.M{"age": 55}})
	c.Find(bson.M{"name": "Kaibb"}).One(&p)
	fmt.Println(p)
}
