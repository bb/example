package main

//goquery 用法示例，以dota2英雄首页为例，获取文档内容
import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
)

func main() {
	fmt.Println("Hello goquery")
	var doc *goquery.Document
	var e error
	//加载DOta2 英雄列表
	if doc, e = goquery.NewDocument("http://www.dota2.com.cn/heroes/index.htm"); e != nil {
		fmt.Println(e)
		panic(e)
	}
	//根据css 选择器查找元素， 查找id 为heroBioDynamic > heroBioName的元素
	su := doc.Find("#heroBioDynamic #heroBioName").Text()
	fmt.Println(su)

	//查找元素属性 class="nav_logo" 的 title 属性值
	nave, _ := doc.Find(".nav_logo").First().Attr("title")
	fmt.Println(nave)
	//查找所有class="heroPickerIconLink" 的元素，并循环处理
	doc.Find(".heroPickerIconLink").Each(func(i int, s *goquery.Selection) {
		src, _ := s.Find(".heroHoverLarge").First().Attr("src")
		fmt.Println(i, src)
	})

}
