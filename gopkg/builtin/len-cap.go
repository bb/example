package main

//@author: www@kaibb.com
//@time: 2013-11-20 11:06:58
//@goPlay: http://play.golang.org/p/NO_6xbmGuV
import (
	"fmt"
)

func main() {
	//len()
	//Array: the number of elements in v.
	//Pointer to array: the number of elements in *v (even if v is nil).
	//Slice, or map: the number of elements in v; if v is nil, len(v) is zero.
	//String: the number of bytes in v.
	//Channel: the number of elements queued (unread) in the channel buffer;
	//if v is nil, len(v) is zero.

	//cap()
	//Array: the number of elements in v (same as len(v)).
	//Pointer to array: the number of elements in *v (same as len(v)).
	//Slice: the maximum length the slice can reach when resliced;
	//if v is nil, cap(v) is zero.
	///Channel: the channel buffer capacity, in units of elements;
	//if v is nil, cap(v) is zero.

	var array [10]int
	//length array:  10
	fmt.Println("length array: ", len(array))
	fmt.Println("cap array: ", cap(array))
	point := &array
	//length Pointer to Array:  10
	fmt.Println("length Pointer to Array: ", len(point))
	fmt.Println("cap Pointer to Array: : ", cap(point))
	var nilArray []int
	//length nil array: 0
	fmt.Println("length nil array: ", len(nilArray))
	//fmt.Println("cap nil array : ",cap(nillArray))
	sl := make([]int, 10)
	//length  slice : 10
	//cap slice : 10
	fmt.Println("length  slice : ", len(sl))
	fmt.Println("cap slice: ", cap(sl))
	sl = nil
	//length nil slice : 0
	fmt.Println("length nil slice : ", len(sl))
	fmt.Println("cap nil slice : ", cap(sl))

	c := make(chan int, 4)
	//length channel : 0
	//cap channel : 4
	fmt.Println("length channel : ", len(c))
	fmt.Println("cap channel : ", cap(c))

}