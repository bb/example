package main

import "fmt"

func main() {
	s1 := make([]int, 0, 100)
	fmt.Println("append 之前")
	for _, v := range s1 {
		fmt.Printf("%d-->", v)
	}

	fmt.Println("\nappend 之后")

	s1 = append(s1, 20)

	for _, v := range s1 {
		fmt.Printf("%d-->", v)
	}

	sm := "I love you!"
	fmt.Printf("\nFirst char: %s\n", sm[:1])
	sm = sm[len(sm)-1:]
	fmt.Printf("second char: %s\n", sm[:1])
}
