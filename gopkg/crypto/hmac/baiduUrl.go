package main
//author: www@kaibb.com
//2013-11-19 17:47:51
//Online: http://play.golang.org/p/ed_kfBHuZk
import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"net/url"
)

// hmac, sha1,base64 encode加密。 百度在线URL签名 http://developer.baidu.com/bae/bcs/key/sign/
//签名算法 http://developer.baidu.com/wiki/index.php?title=docs/cplat/stor/access/signed-url
func main() {
	//Content算法规则示例
	Content := "MBO\nMethod=POST\nBucket=xiaoshengshuo\nObject=/网络玄幻/哈哈.mp3\n"
	fmt.Println(Content)
	//百度云存储Secret Key(SK)：
	key := []byte("A336d5fdf1a345a2d6b23f527es34bb")
	//生成Signature
	h := hmac.New(sha1.New, key)
	h.Write([]byte(Content))
	su := h.Sum(nil)
	//base64加密
	str := base64.StdEncoding.EncodeToString(su)
	//urlEncode
	str = url.QueryEscape(str)
	fmt.Println(str)

}

//result
//MBO
//Method=POST
//Bucket=xiaoshengshuo
//Object=/网络玄幻/哈哈.mp3

//4YffSVe8LGNjdO9pIShTtyOuiro%3D

