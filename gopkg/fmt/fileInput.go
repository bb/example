package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	inf, err := os.Open("D:\\kaibb\\code\\example\\gopkg\\fmt\\fileInput.go")
	if err != nil {
		fmt.Printf("read file error! %v", err)
		return
	}
	defer inf.Close()

	ir := bufio.NewReader(inf)

	for {
		is, rerr := ir.ReadString('\n')
		if rerr == io.EOF {
			return
		}
		fmt.Printf("%s", is)
	}
}
