package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter some input:")
	input, err := inputReader.ReadString('\n')
	if err == nil {
		fmt.Println("the input is:", input)
	}

}
