package main

import "fmt"

func main() {
	var (
		firstName, lastName, s string
		input                  = "56.12 / 5112 / Go"
		format                 = "%f / %d / %s"
		f                      float64
		i                      int
	)
	fmt.Println("please input your full name!")

	fmt.Scanln(&firstName, &lastName)
	fmt.Printf("Hi %s,%s \n", firstName, lastName)

	fmt.Sscanf(input, format, &f, &i, &s)
	fmt.Println("from the string we read: ", f, i, s)
}
