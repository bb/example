package main

//@author: www@kaibb.com
//@time: 2013-12-12 14:35:22
//@goPlay: http://play.golang.org/p/d2AfyKKRA_
//@desc: what slice
//@ref: http://blog.golang.org/slices
import (
	"fmt"
)

func AddOneToEachElement(slice []byte) {
	for i := range slice {
		slice[i]++
	}
}
func SubtractOneFromLength(slice []byte) []byte {
	slice = slice[0 : len(slice)-1]
	return slice
}

func PtrSubtractOneFromLength(slicePtr *[]byte) {
	slice := *slicePtr
	*slicePtr = slice[0 : len(slice)-1]
}
func main() {
	var buffer [256]byte
	slice := buffer[10:20]

	for i := 0; i < len(slice); i++ {
		slice[i] = byte(i)
	}
	fmt.Println("before", slice)
	AddOneToEachElement(slice)
	fmt.Println("after", slice)

	fmt.Println("Before: len(slice) =", len(slice))
	newSlice := SubtractOneFromLength(slice)
	fmt.Println("After:  len(slice) =", len(slice))
	fmt.Println("After:  len(newSlice) =", len(newSlice))

	fmt.Println("Before: len(slice) =", len(slice))
	PtrSubtractOneFromLength(&slice)
	fmt.Println("After:  len(slice) =", len(slice))
}
