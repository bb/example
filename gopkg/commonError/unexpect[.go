package main

//@author: www@kaibb.com
//@time: 2013-11-20 11:14:39
//@goPlay: http://play.golang.org/p/0cfRt2b3OP
import(
	"fmt"
)

func main(){
	// syntax error: unexpected [, expecting semicolon or newline or }
	// 声明array语法: var [n]<type>
	var array int[10]
	fmt.Println(len(array))
}