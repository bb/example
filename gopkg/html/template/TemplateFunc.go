package main

//@author: www@kaibb.com
//@time: 2013-12-06 22:41:32
//@goPlay: http://play.golang.org/p/2JeZhF2RGP
//@desc: template 函数使用
import (
	"fmt"
	"html/template"
	"os"
)

//也可以用管道方法 .Span | toCap
var temp = `<a href="{{.Href}}">{{.Text}}</href> <br/><span id='{{.Id}}'>{{toCap .Span}}</span>`
var temp2 = `<span id='{{.Id}}'>{{.Span}}</span>`

type data struct {
	Href, Text, Id, Span string
}

func toCap(str string) (capStr string) {
	capStr = str + " hello function"
	return
}
func main() {
	//var funcs template.FuncMap
	//funcs = make(template.FuncMap, 0)
	//funcs["toCap"] = toCap

	funcs := template.FuncMap{"toCap": toCap}
	fmt.Println("Hello World!")
	// template.Parse 只能解析一个字符
	//tep, _ := template.New("test").Parse(temp, temp2)
	tep, _ := template.New("test").Funcs(funcs).Parse(temp)
	tep.Execute(os.Stdout, &data{"baidu.com", "百度", "span", "SpanText"})
}
