package main

//@author: www@kaibb.com
//@time: 2013-12-06 17:58:06
//@goPlay: http://play.golang.org/p/B9WOnNBhxQ
//@desc: template 函数使用,range,多值输出
import (
	"fmt"
	"html/template"
	"os"
)

//也可以用管道方法 .Span | toCap
var temp = `<a href="{{.Href}}">{{.Text}}</href> <br/><span id='{{.Id}}'>{{toCap .Span "string2" .Str3}} {{range $k,$v := .Emails}} <li> {{$k}} : {{$v}} </li>{{end}}</span>`
var temp2 = `<span id='{{.Id}}'>{{.Span}}</span>`

type data struct {
	Href, Text, Id, Span, Str3 string
	Emails                     map[string]string
}

func toCap(str1, str2, str3 string) (capStr string) {
	capStr = str1 + "  " + str2 + "  " + str3 + " hello function"
	return
}
func main() {
	//var funcs template.FuncMap
	//funcs = make(template.FuncMap, 0)
	//funcs["toCap"] = toCap

	funcs := template.FuncMap{"toCap": toCap}
	fmt.Println("Hello World!")
	// template.Parse 只能解析一个字符
	//tep, _ := template.New("test").Parse(temp, temp2)
	tep, _ := template.New("test").Funcs(funcs).Parse(temp)
	tep.Execute(os.Stdout, &data{"kaibb.com", "kaibb", "span", "SpanText", "strin3", map[string]string{"kaibb": "kaibb.com", "yjmnrg": "yjmnrg@gmail.com"}})
}
