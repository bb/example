package main

//@author: www@kaibb.com
//@time: 2013-12-06 22:18:06
//@goPlay: http://play.golang.org/p/WkJqWst280
import (
	"fmt"
	"html/template"
	"os"
)

var temp = `<a href="{{.Href}}">{{.Text}}</href>`

type data struct {
	Href, Text string
}

func main() {
	fmt.Println("Hello World!")
	tep, _ := template.New("test").Parse(temp)
	tep.Execute(os.Stdout, &data{"baidu.com", "百度"})
}
