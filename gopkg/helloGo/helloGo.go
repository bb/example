package main

//author: www@kaibb.com
//2013-11-19 17:43:19
//Online: http://play.golang.org/p/t3dEcekoMU
import (
	"fmt"
	"strconv"
)

func main() {
	s := strconv.QuoteRuneToASCII('(')
	fmt.Println(s)
	fmt.Println("Hello Go.........")

	for _, v := range "1111010111001100000110101010100110000010101111010111100" {
		fmt.Printf("%s-->", string(v))
	}
}
