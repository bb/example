package main

import "fmt"

type Queue struct {
	first, last *node
}

type node struct {
	data string
	next *node
}

func NewQueue() *Queue {
	q := &Queue{}
	return q
}

func (q *Queue) Add(data string) {
	node := &node{data, nil}
	if q.first == nil {
		q.first = node
		q.last = node
	} else {
		q.last.next = node
		q.last = node
	}
}

func (q *Queue) Del() (data string, ok bool) {
	if q.first == nil {
		ok = false
	} else {
		ok = true
		data = q.first.data
		q.first = q.first.next
	}
	return
}

func (q *Queue) Print() {
	header := q.first
	for header != nil {
		fmt.Printf("%v-->", header.data)
		header = header.next
	}
	fmt.Println()
}

func main() {
	q := NewQueue()
	q.Add("A")
	q.Add("B")
	q.Add("C")
	q.Add("D")
	q.Add("E")
	q.Add("F")
	q.Add("G")
	q.Add("H")

	q.Print()

	value, _ := q.Del()
	fmt.Printf("del:%v\n", value)
	value, _ = q.Del()
	fmt.Printf("del:%v\n", value)

	q.Print()
}
