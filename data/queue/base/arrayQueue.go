package main

import "fmt"

type Aqueue struct {
	first, last, size int
	data              []string
}

func newAqueue(size int) *Aqueue {
	q := &Aqueue{first: size - 1, last: size - 1, size: 0, data: make([]string, size)}
	return q
}

func (aq *Aqueue) add(data string) {
	if aq.size == len(aq.data) {
		return
	}
	if aq.size == 0 {
		aq.data[aq.first] = data
	} else {
		aq.last = (aq.last + 1) % len(aq.data)
		aq.data[aq.last] = data
	}
	aq.size++
}
func (aq *Aqueue) del() (data string, ok bool) {
	if aq.size == 0 {
		ok = false
		return
	}
	data = aq.data[aq.first]
	aq.first = (aq.first + 1) % len(aq.data)
	aq.size--
	ok = true
	return
}

func (aq *Aqueue) print() {
	for i := 0; i < aq.size; i++ {
		m := (aq.first + i) % len(aq.data)
		fmt.Printf("%v-->", aq.data[m])
	}
	fmt.Println()
}
func main() {
	aq := newAqueue(10)
	aq.add("A")
	aq.add("B")
	aq.add("C")
	aq.add("D")
	aq.add("E")
	aq.print()

	aq.del()
	aq.del()
	aq.print()

	aq.add("D")
	aq.add("E")
	aq.print()
}
