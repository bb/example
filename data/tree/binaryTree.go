package main

import "fmt"

type node struct {
	data        string
	left, right *node
}

/**
新建二叉树
*/
func newBinaryTree(parent string, point string) (bt *node) {
	var data string
	fmt.Printf("please input parent %s %s data:", parent, point)
	fmt.Scanln(&data)
	if data != "" {
		fmt.Printf("new node: %s\n", data)
		bt = &node{}
		bt.data = data
		bt.left = newBinaryTree(data, "left")
		bt.right = newBinaryTree(data, "right")
	}
	return
}

func visit(data string, level int) {
	fmt.Printf("%s on level %d\n", data, level)
}

/**

 */
func (bt *node) preOrderTraverse(level int) {
	if bt != nil {
		visit(bt.data, level)
		bt.left.preOrderTraverse(level + 1)
		bt.right.preOrderTraverse(level + 1)
	}
}

func main() {
	bt := newBinaryTree("root", "root")
	bt.preOrderTraverse(1)
}
