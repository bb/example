package main

import "fmt"

type node struct {
	data   string //数据
	parent int    //双亲位置
}

type tree struct {
	nodes      []node //结点数组
	root, size int    //根结点位置,结点数目
}

func newTree() *tree {
	t := &tree{}
	nodes := make([]node, 30)
	var input string
	fmt.Printf("\ninput root data: ")
	fmt.Scanln(&input)

	nodes = append(nodes, newSub(-1)...)
	t.nodes = nodes
	t.root = -1
	t.size = len(nodes)
	return t
}

func newSub(cur int) []node {
	sub := make([]node, 30)
	var lchild, rchild string
	fmt.Printf("\ninput left data: ")
	fmt.Scanln(&lchild)
	if lchild != "" {
		n := &node{lchild, cur}
		sub = append(sub, *n)
		sub = append(sub, newSub(cur+1)...)
		fmt.Printf("\ninput right data:")
		fmt.Scanln(&rchild)
		if rchild != "" {
			sub = append(sub, newSub(len(sub)-1)...)
		}
	}
	return sub
}

func (t *tree) print() {
	for _, v := range t.nodes {
		fmt.Printf("%s:%d -->", v.data, v.parent)
	}
	fmt.Println()
}
func main() {
	t := newTree()
	t.print()
}
