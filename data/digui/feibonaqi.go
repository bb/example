package main

import "fmt"

/**
斐波那奇数列递归实现
*/
func fbnq(n int) int {
	if n < 2 {
		return n
	}
	return fbnq(n-1) + fbnq(n-2)
}

/**
斐波那奇数列数组实现
*/
func afbnq(n int) []int {
	res := make([]int, n)
	res[0] = 0
	res[1] = 1
	for i := 2; i < n; i++ {
		res[i] = res[i-1] + res[i-2]
	}
	return res
}
func main() {
	for i := 0; i < 20; i++ {
		fmt.Printf("%v->", fbnq(i))
	}
	fmt.Println()
	res := afbnq(20)
	for _, v := range res {
		fmt.Printf("%v->", v)
	}
}
